FROM ubuntu:xenial

RUN apt-get update && \
    apt-get install --no-install-recommends -y \
            openconnect ca-certificates && \
    rm -rf /var/lib/apt/lists/*

COPY entrypoint.sh /

ENTRYPOINT ["sh", "/entrypoint.sh"]
