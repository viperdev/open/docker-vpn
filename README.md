# OpenConnect in Docker!

This image is used to connect to VPNs which OpenConnect can use. You can utilize
the network to another container by making it use this container's network stack.

## How to use

### Method 1: Use Docker run

#### Start the container

```bash
docker run --privileged --name my-vpn-container \
    -e VPN_SERVER=vpn.some-org.tld \
    -e VPN_USERNAME=username -e VPN_PASSWORD=password \
    registry.gitlab.com/viperdev/open/docker-vpn
```

#### Use the container's network stack

```bash
docker run --name something-else \
    --network="container:my-vpn-container" \
    some-image-here
```

Your container will use `my-vpn-container`'s network stack.

### Method 2: Use Compose

```yaml
version: '2'
services:
  vpn:
    image: registry.gitlab.com/viperdev/open/docker-vpn
    container_name: my-vpn-container
    privileged: true
    environment:
      - VPN_SERVER=vpn.some-org.tld
      - VPN_USERNAME=username
      - VPN_PASSWORD=password
  something-else:
    image: some-image-here
    networks:
      - "container:my-vpn-container"
```

`something-else` service will use `vpn` service's network stack.

## Notes

This container requires to be privileged to make a tunnel interface
