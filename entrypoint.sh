#!/bin/sh

if [ -z "$VPN_SERVER" ] || \
       [ -z "$VPN_USERNAME" ] || [ -z "$VPN_PASSWORD" ]; then
    echo "One of the required environment variables is not set." >&2
    exit 1
fi

echo "$VPN_PASSWORD" | openconnect --user="$VPN_USERNAME" "$VPN_SERVER" "$@"
